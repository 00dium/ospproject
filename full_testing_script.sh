# Runs the full test suite required for the project
# format ./programname "input file" "output file" 'circular buffer size' 'block-size(amount fo bytes read from disk)' 
# 'number of reader threads' 'number of writer threads' 'reader thread processing time' 'writer thread proc time' 'implemnation type (1 for standard, 2 for individual locking)' 'console debug 0 = OFF 1 = ON'

# TEST 0 - Exoected behaviour test 
# This test is designed to show that the program is exhibiting the expected behaviour, given a variety of page sizes, number of threads and buffer sizes

echo "-------------------------\nTEST 0 - Expected Behaviour\n-------------------------\n" > "testoutput.txt"
echo "\n There should be NO output if files are the same\n\nBegin Test\n\n" > "testoutput.txt"

#run text file test
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 2048 5 5 0 0 1 0 
#confirm files are the same
diff files/testfile_LARGE.txt files/testoutLARGE.txt
rm -f files/testoutLARGE.txt

#run image file test
./copy files/cosmos.jpg files/cosmos_out.jpg 50 4096 10 10 0 0 1 0
#confirm files are the same
diff files/cosmos.jpg files/cosmos_out.jpg 
rm -f files/cosmos_out.jpg 

#video file test
./copy files/over9000.mp4 files/over9000_out.mp4 50 8192 1 1 5000 50000 1 0
#confirm files are the same
diff files/over9000.mp4 files/over9000_out.mp4
rm -f files/over9000_out.mp4

#run zip file test
./copy files/over9000.zip files/over9000_out.zip 50 128 5 5 0 0 1 0
#confirm files are the same
diff files/over9000.zip files/over9000_out.zip
rm -f files/over9000_out.zip

echo "\n\n Test Completed\n\n" > "testoutput.txt"

# TEST 1 - Optimal page size
# This test is designed to find the optimal page size (bytes read from disk) to observe its impact on performance

echo "-------------------------\nTEST 1 - Optimal page size\n-------------------------\n" > "testoutput.txt"

./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 128 5 5 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 256 5 5 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 512 5 5 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 1024 5 5 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 2048 5 5 0 0 1 0 >> "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 5 5 0 0 1 0 >> "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 8192 5 5 0 0 1 0 >> "testoutput.txt"
rm -f files/testoutLARGE.txt

# TEST 2 - Number of Threads
# This test is designed to see the impact on performance when adding additonal threads to the implemenation
# Note: replace with optimal pagesize 

echo "-------------------------\nTEST 2 - NUMBER OF THREADS: for each implementation\n-------------------------\n" > "testoutput.txt"

echo "-------------------------\nTEST 2.1 - NUMBER OF THREADS: implemenation 1 \n-------------------------\n" > "testoutput.txt"
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 1 1 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 2 2 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 3 3 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 5 5 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 10 10 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 15 15 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 50 50 0 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt

echo "-------------------------\nTEST 2.2 - NUMBER OF THREADS: implemenation 2 \n-------------------------\n" > "testoutput.txt"
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 1 1 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 2 2 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 3 3 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 5 5 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 10 10 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 15 15 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 50 50 0 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt

# TEST 3 - Number of Threads - with processing time for each block
# This test is designed to see the impact on performance when an arbtrary processing time is added for each block of data
# Note: replace with optimal pagesize, wait time in 1/100th of a second

echo "-------------------------\nTEST 3 - NUMBER OF THREADS: for each implementation (with processing time for each block) \n-------------------------\n" > "testoutput.txt"

echo "-------------------------\nTEST 3.1 - NUMBER OF THREADS: with proc implemenation 1 \n-------------------------\n" > "testoutput.txt"
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 1 1 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 2 2 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 3 3 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 5 5 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 10 10 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 15 15 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 50 50 10000000 0 1 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt

echo "-------------------------\nTEST 3.2 - NUMBER OF THREADS: with proc implemenation 2 \n-------------------------\n" > "testoutput.txt"
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 1 1 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 2 2 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 3 3 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 5 5 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 10 10 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 15 15 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
./copy files/testfile_LARGE.txt files/testoutLARGE.txt 20 4096 50 50 10000000 0 2 0 > "testoutput.txt"
rm -f files/testoutLARGE.txt
