# Home directory 
BASE =.
# Compile flags
FLAGS0 = -lpthread
FLAGS1 = -ansi -Wall -pedantic -lpthread -lrt
FLAGS2 = -ansi -lpthread -lrt
FLAGSDEBUG = -ansi -Wall -pedantic -lpthread -lrt -g -O2
# Compiler
CC = gcc

# Vars for archive
USER = s3233820_
ASS = A1
EXT = .zip

all : copy

debug : d-copy

#production program

copy : fcopy.o fcopy_util.o semaphore.o 
	$(CC) $(FLAGS1) -o copy fcopy.o fcopy_util.o semaphore.o

fcopy.o : ./src/fcopy.c ./src/fcopy.h ./src/fcopy_util.h ./src/semaphore.h
	$(CC) $(FLAGS1) -c ./src/fcopy.c

fcopy_util.o : ./src/fcopy_util.c ./src/fcopy_util.h ./src/fcopy.h
	$(CC) $(FLAGS1) -c ./src/fcopy_util.c

# compile semaphore code (no -wall -pedantic)
semaphore.o : ./src/semaphore.c ./src/semaphore.h
	$(CC) $(FLAGS2) -c ./src/semaphore.c

#debug program (performance issues vs production)

#d-copier : d-fcopy.o d-fcopy_util.o d-semaphore.o 
#	$(CC) $(FLAGSDEBUG) -o copier fcopy.o fcopy_util.o semaphore.o

#d-fcopy.o : ./src/fcopy.c ./src/fcopy.h ./src/fcopy_util.h ./src/semaphore.h
#	$(CC) $(FLAGSDEBUG) -c ./src/fcopy.c

#d-fcopy_util.o : ./src/fcopy_util.c ./src/fcopy_util.h ./src/fcopy.h
#	$(CC) $(FLAGSDEBUG) -c ./src/fcopy_util.c

# compile semaphore code (no -wall -pedantic)
#d-semaphore.o : ./src/semaphore.c ./src/semaphore.h
#	$(CC) $(FLAGSDEBUG) -c ./src/semaphore.c
	
	
# archive using UNIX zip utility
archive:
	zip $(USER)$(ASS)$(EXT) fcopy.c fcopy.h \
							fcopy_util.c fcopy_util.h \
							makefile \
							semaphore.c semaphore.h \
							files/

clean:
	rm -rf *o copy
	rm -rf *o fcopy.o
	rm -rf *o fcopy_util.o
