/******************************************************************************
*
*
*
* Full Name        : Christopher White
* Student Number   : s3233820
* Yallara Username : s3233820
* Course Code      : 
* Program Code     : BP097 - Bachelor of Computer Science
* 
******************************************************************************/

#ifndef _FCOPY_UTIL_H
#define _FCOPY_UTIL_H


/* input-output functions */
char * readLineFromFile(file_t * file);
int writeLineToFile(file_t * file, char * lineToFile);
block_t * readBlockFromFile(file_t * file, int BLOCK_SIZE);
int writeBlockToFile(file_t * file, block_t * block);

/* memory handling functions */
void * mallocMem(int size);

/* text and display oriented functions */
void printfDebug(int DEBUG, char * format, ...);
void printIt(char * format, va_list args);
void printfErr (char * format, ...);
void printfLog (char * format, ...);
char * timeStamp();
void headerGraphic();



#endif
