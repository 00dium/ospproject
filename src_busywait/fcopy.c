/******************************************************************************
*
*
*
* Full Name        : Christopher White
* Student Number   : s3233820
* Yallara Username : s3233820
* Course Code      : 
* Program Code     : BP097 - Bachelor of Computer Science
* 
******************************************************************************/

#include "fcopy.h"
#include "fcopy_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <time.h>

/* Global program flags */					
int writer_eof_flag;
int reader_eof_flag;
int GLOBAL_DEBUG;

/* thread return code constants */
int READER_EOF;
int WRITER_EOF;

/* program environment variables structure */
copier_t copy;

/* accessed by all threads, protected by sharedBuf->mtxBuf */
int BUF_SIZE = 24;

/****************************************************************************
* 								THREADS
****************************************************************************/

/* reader thread implementation 1*/
void* reader_thread1(void *arg)
{
	/* char * sLineFromFile; */
	block_t * blockFromFile;
	threadargs_t * t_args;
	copier_t * copy;
	timing_t * time;
	file_t * inFile;
	buffer_t * sharedBuf;
	int numRead;
	int DEBUG;
	int BLOCK_SIZE;
	struct timespec tm;
	
	clock_t start_time,start_wait;
	
	/* map local variables from t->args & copier_t struct */
	t_args = (threadargs_t *)arg;
	copy = t_args->copy;
	inFile = &copy->inFile;
	time = t_args->timing;
	sharedBuf = &copy->sharedBuf;
	DEBUG = t_args->DEBUG_ON;
	BLOCK_SIZE = t_args->BLOCK_SIZE;
	
	printfDebug(DEBUG, "Reader thread with TID: %i created\n", (int)pthread_self(), (int)getpid());
	start_time = clock();
	
	tm.tv_sec = 0;
	tm.tv_nsec = t_args->proc_ns;
	
	/* until eof_flag is set */
	while (FOREVER){
		
		/******************************************************
		* 					READ FROM FILE
		*******************************************************/
		
		/* lock access to file */
		printfDebug(DEBUG, "Reader TID %d: Attempting to lock inputFile mutex\n", (int)pthread_self());
		
		/* lock file and record timings */
		start_wait = clock();
		pthread_mutex_lock(&inFile->mtxFile);
		time->totalWait = time->totalWait + (clock() - start_wait);
		
		printfDebug(DEBUG, "Reader TID %d: Has locked the inputFile mutex\n", (int)pthread_self());
		
		if (reader_eof_flag == FALSE){
			
			/* Read block from file  (returns pointer to a block_t struct)*/
			blockFromFile = readBlockFromFile(inFile, BLOCK_SIZE);
			nanosleep(&tm, NULL);
			
			/* check if end of file */
			if (feof(inFile->file) != 0){
				/* Sets EOF */
				printfDebug(DEBUG, "Reader TID %d: EOF detected on file stream, setting reader_eof_flag\n", (int)pthread_self());
				reader_eof_flag = TRUE;
			}
			
			/******************************************************
			* 				INSERT INTO BUFFER
			*******************************************************/
			
			/* write file block if more than 1 bytes of data was read (prevents the case that EOF was the first character read from the file) */
			if (blockFromFile->BLOCK_SIZE > 0){
				
				/* increment the value of blocksRead */
				inFile->blocksRead = inFile->blocksRead + 1;
				
				/* decrememnt semaphore - block if s->v <= 0 (no room in buffer) */
				printfDebug(DEBUG, "Reader TID %d: down(emptyCount)\n", (int)pthread_self());
				semaphore_down(&sharedBuf->semEmptyCount);
				
				/* add element to the bufer */
				printfDebug(DEBUG, "Reader TID %d: Adding element to buffer\n", (int)pthread_self());
				addToBuffer(sharedBuf, blockFromFile);
				
				/* increment fillCount semaphore - signal waiting writer threads of change */
				printfDebug(DEBUG, "Reader TID %d: up(fillCount)\n", (int)pthread_self());
				semaphore_up(&sharedBuf->semFillCount);
			}
			
			/* if EOF was read on the last call to readBlockFromFile() */
			if (reader_eof_flag == TRUE){
				/* decrememnt semaphore - block if s->v <= 0 (no room in buffer) */
				printfDebug(DEBUG, "Reader TID %d: down(emptyCount)\n", (int)pthread_self());
				semaphore_down(&sharedBuf->semEmptyCount);
				
				/* add element to the bufer */
				printfDebug(DEBUG, "Reader TID %d: Adding NULL entry to buffer (EOF-flag) \n", (int)pthread_self());
				addToBuffer(sharedBuf, NULL);
				
				/* increment fillCount semaphore - signal waiting writer threads of change */
				printfDebug(DEBUG, "Reader TID %d: up(fillCount)\n", (int)pthread_self());
				semaphore_up(&sharedBuf->semFillCount);
			}
		}
		
		/* unlock access to file */
		printfDebug(DEBUG, "Reader TID %d: Has unlocked the inputFile mutex\n", (int)pthread_self());
		pthread_mutex_unlock(&inFile->mtxFile);
		
		/* Check if EOF is set by another thread */
		if (reader_eof_flag == TRUE){
			printfDebug(DEBUG, "Reader TID %d: EOF flag has been set\n", (int)pthread_self());
			
			/* free lock on mutex */
			pthread_mutex_unlock(&inFile->mtxFile);
			
			t_args->t_rc = READER_EOF;
			time->run = clock() - start_time;
			/* terminate and signal to main() that process has completed */
			pthread_exit((void *)&t_args->t_rc);
		}
	}
	
	/* pthread_exit((void *)0); */
}

/* reader thread implementation 1*/
void* reader_thread2(void *arg)
{
	/* char * sLineFromFile; */
	block_t * blockFromFile;
	threadargs_t * t_args;
	copier_t * copy;
	timing_t * time;
	file_t * inFile;
	buffer_t * sharedBuf;
	int numRead;
	int DEBUG;
	int BLOCK_SIZE;
	int blockNumber;
	int eof_encountered;
	struct timespec tm; 
	
	eof_encountered = FALSE;
	clock_t start_time,start_wait;
	
	/* map local variables from t->args & copier_t struct */
	t_args = (threadargs_t *)arg;
	copy = t_args->copy;
	inFile = &copy->inFile;
	time = t_args->timing;
	sharedBuf = &copy->sharedBuf;
	DEBUG = t_args->DEBUG_ON;
	BLOCK_SIZE = t_args->BLOCK_SIZE;
	
	printfDebug(DEBUG, "Reader thread with TID: %i created\n", (int)pthread_self(), (int)getpid());
	start_time = clock();
	
	tm.tv_sec = 0;
	tm.tv_nsec = tm.tv_nsec = t_args->proc_ns;
	
	/* until eof_flag is set */
	while (FOREVER){
		
		/******************************************************
		* 					READ FROM FILE
		*******************************************************/
		
		/* lock access to file */
		printfDebug(DEBUG, "Reader TID %d: Attempting to lock inputFile mutex\n", (int)pthread_self());
		
		/* lock file and record timings */
		start_wait = clock();
		pthread_mutex_lock(&inFile->mtxFile);
		time->totalWait = time->totalWait + (clock() - start_wait);
		
		printfDebug(DEBUG, "Reader TID %d: Has locked the inputFile mutex\n", (int)pthread_self());
		
		if (reader_eof_flag == FALSE){
			
			/* Read block from file  (returns pointer to a block_t struct)*/
			blockFromFile = readBlockFromFile(inFile, BLOCK_SIZE);
			
			/* check if end of file */
			if (feof(inFile->file) != 0){
				/* Sets EOF */
				printfDebug(DEBUG, "Reader TID %d: EOF detected on file stream, setting reader_eof_flag\n", (int)pthread_self());
				reader_eof_flag = TRUE;
				eof_encountered = TRUE;
			}
			
			/* increment the value of blocksRead */
			blockNumber = inFile->blocksRead;
			inFile->blocksRead = inFile->blocksRead + 1;
			
			/* unlock access to file */
			printfDebug(DEBUG, "Reader TID %d: Has unlocked the inputFile mutex\n", (int)pthread_self());
			pthread_mutex_unlock(&inFile->mtxFile);
			
			nanosleep(&tm, NULL);
			
			/******************************************************
			* 				INSERT INTO BUFFER
			*******************************************************/
			
			/* write file block if more than 1 bytes of data was read (prevents the case that EOF was the first character read from the file) */
			if (blockFromFile->BLOCK_SIZE > 0){
				
				/**************************************************************
				* 				NEW CODE (pthread_mutex_cond) can be used here instead (more efficient)
				**************************************************************/
				/* continues to yield the processor to another thread until the block held by this 
				thread is ready to be inserted into the buffer */
				pthread_mutex_lock(&sharedBuf->mtxBuf);
				while (sharedBuf->circNextLineToBuffer != blockNumber){
					pthread_mutex_unlock(&sharedBuf->mtxBuf);
					sched_yield();
					pthread_mutex_lock(&sharedBuf->mtxBuf);
				}
				pthread_mutex_unlock(&sharedBuf->mtxBuf);
				
				/******************************************************
				* 				END NEW CODE
				*******************************************************/
				
				/* decrememnt semaphore - block if s->v <= 0 (no room in buffer) */
				printfDebug(DEBUG, "Reader TID %d: down(emptyCount)\n", (int)pthread_self());
				semaphore_down(&sharedBuf->semEmptyCount);
				
				/* add element to the bufer */
				printfDebug(DEBUG, "Reader TID %d: Adding element to buffer\n", (int)pthread_self());
				addToBuffer(sharedBuf, blockFromFile);
				
				/* increment fillCount semaphore - signal waiting writer threads of change */
				printfDebug(DEBUG, "Reader TID %d: up(fillCount)\n", (int)pthread_self());
				semaphore_up(&sharedBuf->semFillCount);
			}
			
			/* PROBLEM: multiple threads could execute this block (only the thread that encountered the EOF flag should enter it) */
			
			
			/* if EOF was read on the last call to readBlockFromFile() */
			if (reader_eof_flag == TRUE && eof_encountered == TRUE){
				/* decrememnt semaphore - block if s->v <= 0 (no room in buffer) */
				printfDebug(DEBUG, "Reader TID %d: down(emptyCount)\n", (int)pthread_self());
				semaphore_down(&sharedBuf->semEmptyCount);
				
				/* add element to the bufer */
				printfDebug(DEBUG, "Reader TID %d: Adding NULL entry to buffer (EOF-flag) \n", (int)pthread_self());
				addToBuffer(sharedBuf, NULL);
				
				/* increment fillCount semaphore - signal waiting writer threads of change */
				printfDebug(DEBUG, "Reader TID %d: up(fillCount)\n", (int)pthread_self());
				semaphore_up(&sharedBuf->semFillCount);
			}
		}else {			
			/* unlock access to file */
			printfDebug(DEBUG, "Reader TID %d: Has unlocked the inputFile mutex\n", (int)pthread_self());
			pthread_mutex_unlock(&inFile->mtxFile);
			
		}
		
		/* Check if EOF is set by another thread */
		if (reader_eof_flag == TRUE){
			printfDebug(DEBUG, "Reader TID %d: EOF flag has been set\n", (int)pthread_self());
			
			t_args->t_rc = READER_EOF;
			time->run = clock() - start_time;
			/* terminate and signal to main() that process has completed */
			pthread_exit((void *)&t_args->t_rc);
		}
	}
	
	/* pthread_exit((void *)0); */
}

/* writer thread implementation 1 */
void* writer_thread1(void *arg)
{
	/* char * sLineToFile; */
	block_t * block;
	threadargs_t * t_args;
	copier_t * copy;
	timing_t * time;
	file_t * outFile;
	buffer_t * sharedBuf;
	int DEBUG;
	struct timespec tm; 
	
	clock_t start_time,start_wait;
	
	/* map local variables to copier struct */
	t_args = (threadargs_t *)arg;
	copy = t_args->copy;
	outFile = &copy->outFile;
	time = t_args->timing;
	sharedBuf = &copy->sharedBuf;
	DEBUG = t_args->DEBUG_ON;
	
	tm.tv_sec = 0;
	tm.tv_nsec = tm.tv_nsec = t_args->proc_ns;
	
	printfDebug(DEBUG, "Writer thread with TID: %i created\n", (int)pthread_self(), (int)getpid());
	start_time = clock();
	
	while (FOREVER){
		
		/* lock access to file */
		printfDebug(DEBUG, "Writer TID %d: Attempting to lock outputFile mutex\n", (int)pthread_self());
		start_wait = clock();	
		pthread_mutex_lock(&outFile->mtxFile);
		time->totalWait = time->totalWait + (clock() - start_wait);	
		printfDebug(DEBUG, "Writer TID %d: Has locked the outputFile mutex\n", (int)pthread_self());
		
		/* check writer EOF not previously set by another thread */
		if (writer_eof_flag == FALSE){
			
			/******************************************************
			* 				READ LINE FROM BUFFER
			*******************************************************/
			
			/* decrememnt semaphore - block if s->v <= 0 (no data in buffer) */
			semaphore_down(&sharedBuf->semFillCount);
			printfDebug(DEBUG, "Writer TID %d: down(fillCount)\n", (int)pthread_self());
			
			/* retrieve line from end of buffer */
			block = removeFromBuffer(sharedBuf);
			printfDebug(DEBUG, "Writer TID %d: removing line from buffer\n", (int)pthread_self());
			
			nanosleep(&tm, NULL);
			
			/* increment emptyCount semaphore - signal waiting reader threads of change */
			semaphore_up(&sharedBuf->semEmptyCount);
			printfDebug(DEBUG, "Writer TID %d: up(emptyCount)\n", (int)pthread_self());
			
			/* check whether block read from buffer is NULL (indicating EOF) */
			if (block == NULL){
				printfDebug(DEBUG, "Writer TID %d: EOF recieved, writer_eof_flag set, terminating thread\n", (int)pthread_self());
				writer_eof_flag = TRUE;
			}else {
				/******************************************************
				* 				WRITE LINE TO FILE
				*******************************************************/
				printfDebug(DEBUG, "Writer TID %d: Writing line to file\n", (int)pthread_self());
				writeBlockToFile(outFile, block);
				
				/* free the memory allocated for this block */
				freeBlock(block);
			}
		}
		
		/* unlock access to file */
		pthread_mutex_unlock(&outFile->mtxFile);
		printfDebug(DEBUG, "Writer TID %d: Unlocked outputFile mutex \n", (int)pthread_self());
		
		/* terminate if EOF flag has been set */
		if (writer_eof_flag == TRUE){
			printfDebug(DEBUG, "Writer TID %d: Writer EOF set terminating thread\n", (int)pthread_self());
			
			/* set return argument and close */
			time->run = clock() - start_time;
			t_args->t_rc = WRITER_EOF;
			pthread_exit((void *)&t_args->t_rc);
		}
	}
	
	/* pthread_exit((void *)&0); */
}


int main(int argc, char* argv[])
{
	/** thread variables **/
	long threads;										/* thread counter */
	
	/* thread arguments, id's and attributes */
	pthread_t* readers;									/* stores an array of thread writer ID's */	
	pthread_t* writers;									/* stores an array of thread reader ID's */	
	threadargs_t* rt_args;
	threadargs_t* wt_args;
	pthread_attr_t attr;								/* used to set IMPLICIT join to threads */
	
	/* thread status and return codes */
	int threadResult;
	int rc;
	void *status;
	
	/* timing variables */
	double average = 0;
	double total_run = 0;
	double total_wait = 0;
	clock_t starting_time1;
	
	int BLOCK_SIZE = 2048;
	int NUM_READERS = 3;
	int NUM_WRITERS = 3;
	
	int R_WAIT_TIME = 0;     /* an proccessing time in seconds, occurs after reading a block from file and placing in the queue */
	int W_WAIT_TIME = 0;     /* an proccessing time in seconds, occurs after reading a block from file and placing in the queue */
	int imp_type = 1;		/* The type of implementation being used, 1 for standard locking, 2 for seperate file and buffer locks */
	
	/* file related variables */
	char inFileName[MAX_FILENAME_LEN];					/* file to be read */
	char outFileName[MAX_FILENAME_LEN];					/* file to be read */
	
	headerGraphic();
	
	/****************************************************************************
	*		get command line arguments
	****************************************************************************/
	
	starting_time1 = clock();
	
	/* check the command line arguments */
	if (argc == 11){
		/* checks file can be opened for reading (R_OK) */
		if ((access(argv[1], R_OK)) != NOERROR){
			printfErr("Cannot open file for reading reason: %s\n", strerror(errno));
		}
		
		/* retrieve arguments */
		strcpy(inFileName, argv[1]);
		strcpy(outFileName, argv[2]);
		
		/* buffer size */
		BUF_SIZE = atoi(argv[3]);
		
		/* block size - size of data read from file (in bytes) */
		BLOCK_SIZE = atoi(argv[4]);
		
		/* num readers & num writers */
		NUM_READERS = atoi(argv[5]);
		NUM_WRITERS = atoi(argv[6]);
		
		R_WAIT_TIME = atoi(argv[7]);
		W_WAIT_TIME = atoi(argv[8]);
		
		imp_type = atoi(argv[9]);
		if (imp_type > 2 || imp_type < 1){
			printfErr("Only 1 or 2 may be used as implementation type\n");
		}
		
		/* debug on/off */
		GLOBAL_DEBUG = atoi(argv[10]);
	}
	else {
		printfErr("Incorrect command line argument format!\n");
		printfErr("\t use: %s <input file> <output file> <buffer size> <block size> <num readers> <num writers> <reader_wait> <writer_wait> <imp_type 1 or 2> <console-debug: 0(OFF) or 1(ON)>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	printf("BUF_SIZE = %i, BLOCK_SIZE = %i NUM_WRITERS = %i, NUM_READERS = %i\n\n", BUF_SIZE, BLOCK_SIZE, NUM_WRITERS, NUM_READERS);
	
	/****************************************************************************
	*	initialise thread timings array
	****************************************************************************/
	
	/* allocate memory for the appropriate number of threads */
	initTimings(&copy.readerTimes, &copy.writerTimes, NUM_READERS, NUM_WRITERS); 
	
	/* initialse data for thread id's */
	readers = (pthread_t*)mallocMem(sizeof(pthread_t) * NUM_READERS);
	writers = (pthread_t*)mallocMem(sizeof(pthread_t) * NUM_WRITERS);
	
	/****************************************************************************
	*	Create and initialise semaphores 
	****************************************************************************/
	
	/* create semEmptyCount semaphore */
	semaphore_init(&(copy.sharedBuf.semEmptyCount));
	/* initialise semaphore value to BUF_SIZE */
	copy.sharedBuf.semEmptyCount.v = BUF_SIZE;
	
	/* create semFillCount semaphore */
	semaphore_init(&(copy.sharedBuf.semFillCount));
	/* initialise semaphore value to 0 */
	copy.sharedBuf.semFillCount.v = 0;
	
	/****************************************************************************
	*			Setup public data structures (Accessed by Threads)
	****************************************************************************/
	
	initGlobals();
	initBuf(&copy.sharedBuf, BUF_SIZE);
	
	/* Input file */
	initFile(inFileName, &copy.inFile);
	
	/* Output file */
	initFile(outFileName, &copy.outFile);	
	
	/* open input file */
	if ((copy.inFile.file = fopen(copy.inFile.fileName, "r")) == NULL){
		printfErr("Could not open input file, reason: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	/* create and open output file (truncate if exists) */
	if ((copy.outFile.file = fopen(copy.outFile.fileName, "w+")) == NULL){
		printfErr("Could not create output file, reason: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	/****************************************************************************
	*		Initialise thread argument structures
	****************************************************************************/
	
	/* allocate memory for the structures that will hold the data passed to the thread 
		NOTE: This is necessarry due to the fact a thread start routine can only 
		take a single (void *) as an argument, thus pass by value is not possible 
		so each thread must have memory on the heap (or stack) that is not modified 
		by main()*/
	rt_args = (threadargs_t*)mallocMem(sizeof(threadargs_t) * NUM_READERS);
	wt_args = (threadargs_t*)mallocMem(sizeof(threadargs_t) * NUM_WRITERS);
	
	/****************************************************************************
	*		create threads
	****************************************************************************/	
	
	/* obtain lock on input file to block reader threads until all threads have been created */
	pthread_mutex_lock(&copy.inFile.mtxFile);
	
	/* Initialize and set thread detached attribute */
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	/* create reader threads passing reference to input file (pass pointer to threads index in array of timingStructs) */
	for (threads = 0; threads < NUM_READERS; threads++){
		
		/* init threadargs */
		rt_args[threads].copy = &copy;
		rt_args[threads].threadid = threads;
		rt_args[threads].timing = &copy.readerTimes[threads];
		rt_args[threads].DEBUG_ON = GLOBAL_DEBUG;
		rt_args[threads].BLOCK_SIZE = BLOCK_SIZE;
		wt_args[threads].proc_ns = R_WAIT_TIME;

		if (imp_type == 1){
			rc = pthread_create(&readers[threads], &attr, reader_thread1, (void *)&rt_args[threads]);
		}else if (imp_type == 2){
			rc = pthread_create(&readers[threads], &attr, reader_thread2, (void *)&rt_args[threads]);
		}
		
		/* rc = pthread_create(&readerThreads[threads], &attr, reader_thread, (void *)&inFile); */
		if (rc){
			printfErr("Failed to create new reader thread!\n");
			exit(EXIT_FAILURE);
		}
	}
	
	/* create writer threads passing reference to output file */
	for (threads = 0; threads < NUM_WRITERS; threads++){
		
		/* init threadargs */
		wt_args[threads].copy = &copy;
		wt_args[threads].threadid = threads;
		wt_args[threads].timing = &copy.writerTimes[threads];
		wt_args[threads].DEBUG_ON = GLOBAL_DEBUG;
		wt_args[threads].proc_ns = W_WAIT_TIME;
		
		threadResult = pthread_create(&writers[threads], &attr, writer_thread1, (void *)&wt_args[threads]);
		if (threadResult){
			printfErr("Failed to create new writer thread!\n");
			exit(EXIT_FAILURE);
		}
	}
	
	/* unlock file, allowing reader threads to start */
	pthread_mutex_unlock(&copy.inFile.mtxFile);
	
	/****************************************************************************
	*		Wait for threads to finish executing
	****************************************************************************/
	
	/* Wait for reading threads to close */
	pthread_attr_destroy(&attr);
	for(threads = 0; threads < NUM_READERS; threads++) {
		
		printfLog("READER: Waiting for Reader thread %ld (TID %d) to terminate\n", threads, readers[threads]);
		rc = pthread_join(readers[threads], &status);
		if (rc != 0) {
			printfLog("READER: Thread does not exist or has already completed\n");
		}
		printfLog("READER: Join of thread %ld (TID %d) completed, thread returned: %d\n", threads, readers[threads], rt_args[threads].t_rc);
	}
	
	/* Wait for writer threads to finish */
	for(threads = 0; threads < NUM_WRITERS; threads++) {
		
		printfLog("WRITER: Waiting for Writer thread %ld (TID %d) to terminate\n", threads, writers[threads]);
		rc = pthread_join(writers[threads], &status);
		if (rc != 0) {
			printfLog("WRITER: Thread does not exist or has already completed\n");
		}
		printfLog("WRITER: Join of thread %ld (TID %d) completed, thread returned: %ld\n", threads, writers[threads], wt_args[threads].t_rc);
		
	}
	
	headerGraphic(); 
	
	printf("\n----- Test Breakdown -----\n\n");

	printf("Total Program Time:     %10.0f ms (%.1f sec)\n",(double)(clock()-starting_time1)*1000/CLOCKS_PER_SEC, (double)(clock()-starting_time1)/CLOCKS_PER_SEC);
	printf("Total Blocks Read:       %10.0f lines\n",(double)copy.inFile.blocksRead);
	printf("No of Input Threads:    %10.0f\n",(double)NUM_READERS);
	printf("No of Output Threads:   %10.0f\n",(double)NUM_WRITERS);
	
	printf("\n----- Input Threads -----\n\n");
	
	for(threads = 0; threads < NUM_READERS; threads++)
	{
		printf("Thread %ld Run Time:      %10.0f ms\n",threads+1,(double)(rt_args[threads].timing->run*1000)/CLOCKS_PER_SEC);
		printf("Thread %ld Idle Time:     %10.0f ms\n",threads+1,(double)(rt_args[threads].timing->totalWait*1000)/CLOCKS_PER_SEC);
		
		average = (rt_args[threads].timing->totalWait*1000)/CLOCKS_PER_SEC;
		average = average/((rt_args[threads].timing->run*1000)/CLOCKS_PER_SEC);
		average = average * 100;
		
		printf("Thread %ld Idle %%:        %10.0f %% \n\n",threads+1,(double)average);
		
		total_run += (rt_args[threads].timing->run*1000)/CLOCKS_PER_SEC;
		total_wait = total_wait + ((rt_args[threads].timing->totalWait*1000)/CLOCKS_PER_SEC);
		
		average = 0;
	}
	
	printf("Total Avg Run Time:     %10.0f ms\n",total_run);
	printf("Total Avg Idle Time:    %10.0f ms\n",total_wait); 
	average = total_wait / total_run;
	average = average * 100;
	printf("Average Idle %%:         %10.0f %%\n",average);

	
	total_run = 0;
	total_wait = 0;
	average = 0;
	printf("\n----- Output Threads -----\n\n");
	
	for(threads = 0; threads < NUM_WRITERS; threads++)
	{
		printf("Thread %ld Run Time:      %10.0f ms\n",threads+1,(double)(wt_args[threads].timing->run*1000)/CLOCKS_PER_SEC);
		printf("Thread %ld Idle Time:     %10.0f ms\n",threads+1,(double)(wt_args[threads].timing->totalWait*1000)/CLOCKS_PER_SEC);
		
		average = (wt_args[threads].timing->totalWait*1000)/CLOCKS_PER_SEC;
		average = average/((wt_args[threads].timing->run*1000)/CLOCKS_PER_SEC);
		average = average * 100;
		
		printf("Thread %ld Idle %%:        %10.0f %% \n\n",threads+1,average);
		
		total_run = total_run + ((wt_args[threads].timing->run*1000)/CLOCKS_PER_SEC);
		total_wait = total_wait + ((wt_args[threads].timing->totalWait*1000)/CLOCKS_PER_SEC);
		
		average = 0;
	}
	
	printf("Total Avg Run Time:     %10.0f ms\n",total_run);
	printf("Total Avg Idle Time:    %10.0f ms\n",total_wait);
	average = (total_wait/total_run)*100;
	printf("Average Idle %%:         %10.0f %%\n",average);
	
	printfLog("\n\nCleaning up resources...\n");
	
	/* close mutual exclusion semaphores */
	pthread_mutex_destroy(&copy.inFile.mtxFile);
	pthread_mutex_destroy(&copy.outFile.mtxFile);
	pthread_mutex_destroy(&copy.sharedBuf.mtxBuf);
	
	/* TODO: free memory for thread arguments and timings */
	
	
	/* close counting semaphores */
	semaphore_destroy(&copy.sharedBuf.semEmptyCount);
	semaphore_destroy(&copy.sharedBuf.semFillCount);
	
	printfLog("Closing files...\n");
	
	/* close files */
	fclose(copy.inFile.file);
	fclose(copy.outFile.file);
	
	printfLog("Exiting...\n");
	
	exit(EXIT_SUCCESS);
}

/****************************************************************************
* 						Initialization functions
****************************************************************************/

void initGlobals(){

	writer_eof_flag = FALSE;
	reader_eof_flag = FALSE;

	/* thread return code constants */
	READER_EOF = 1;
	WRITER_EOF = 2;
}

void initFile(char fileName[], file_t * file){
	strcpy(file->fileName, fileName);
	file->blocksRead = INIT;
	file->nextBufBlock = INIT;
	pthread_mutex_init(&file->mtxFile, NULL);
}

int initTimings(timing_t ** readerTimes, timing_t ** writerTimes, int numReaders, int numWriters){
	
	*readerTimes = (timing_t *) mallocMem(sizeof(timing_t) * numReaders);
	*writerTimes = (timing_t *) mallocMem(sizeof(timing_t) * numWriters);
	
	return 1;
}

/****************************************************************************
* 						Buffer Routines
****************************************************************************/

void initBuf(buffer_t * sharedBuf, int BUFFER_SIZE){
	int i;
	
	/* malloc mem for sharedBuf->circBuffer (array of block_t struct pointers) */
	sharedBuf->circBuffer = (block_t **) mallocMem(sizeof(block_t *) * BUFFER_SIZE);
	
	for (i = 0; i < BUFFER_SIZE; i++){
		sharedBuf->circBuffer[i] = NULL;
	}
	
	sharedBuf->circStart = 0;			/* Represents the front of the buffer(where data is read from) */
	sharedBuf->circEnd = 0;				/* Respresents the end of the buffer(where data is written to) */
	sharedBuf->circNextLineToBuffer = 0;
	sharedBuf->circNextLineToFile = 0;
	sharedBuf->count = 0;
	
	/* initialise and set mutex */
	pthread_mutex_init(&sharedBuf->mtxBuf, NULL);
}

/** Adds an element to the shared buffer, block can be NULL in which case, null is added to the buffer indicating eof **/
void addToBuffer(buffer_t * sharedBuf, block_t * block){
	buffer_t * buf = sharedBuf;
	
	pthread_mutex_lock(&buf->mtxBuf);
	
	/* insert item into buffer */
	buf->circBuffer[buf->circEnd] = block;
	
	/* update end of list (the location next data item will be stored) */
	/* if next insert is over the highest index CIRC_BUFFER_SIZE - 1 */
	if (buf->circEnd == (BUF_SIZE - 1)){
		buf->circEnd = 0;
	}else {
		buf->circEnd++;
	}
	
	sharedBuf->count++;
	sharedBuf->circNextLineToBuffer++;
	
	pthread_mutex_unlock(&buf->mtxBuf);
}

/** removes an element from the shared buffer **/
block_t * removeFromBuffer(buffer_t * sharedBuf){
	block_t * block;
	
	pthread_mutex_lock(&sharedBuf->mtxBuf);
	
	/* remove item from buffer */
	block = sharedBuf->circBuffer[sharedBuf->circStart];
	sharedBuf->circBuffer[sharedBuf->circStart] = NULL;
	
	/* update front of list (the location next item will be read from) */
	/* if next  */
	if (sharedBuf->circStart == (BUF_SIZE - 1)){
		sharedBuf->circStart = 0;
	}else {
		sharedBuf->circStart++;
	}
	
	sharedBuf->count--;
	
	pthread_mutex_unlock(&sharedBuf->mtxBuf);
	
	return block;
}

/* free the block */
void freeBlock(block_t * block){
	free(block->mem);
	free(block);
}
