/******************************************************************************
*
*
*
* Full Name        : Christopher White
* Student Number   : s3233820
* Yallara Username : s3233820
* Course Code      : 
* Program Code     : BP097 - Bachelor of Computer Science
* 
******************************************************************************/
/* #ifndef _FCOPY_H
#define _FCOPY_H */

#include "semaphore.h"
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>

#include <stdarg.h>			/* Required for the use of vprintf() */
#include <sys/time.h>		/* getTimeOfDay() etc */
#include <sys/stat.h>
#include <errno.h>			/* defines the errno variables and symbolic constants */
#include <fcntl.h>			/* file control options and constants */
#include <pthread.h>
#include <unistd.h>			/* getpid() etc */

/* general arithmetic constants */
#define TRUE 1
#define FALSE 0
#define FOREVER 1

/* status code sonstants [[ used to determine success/failure of a function ]] */
#define SUCCESS 1
#define FAILURE -1
#define NOERROR 0
#define INIT 0
#define RESET 0

/* thread constants */
#define T_MAX_READERS 3
#define T_MAX_WRITERS 3

/* string & data strcutre defining constants */
#define MAXBUF 1024
#define CIRC_BUFFER_SIZE 16
#define MAX_FILENAME_LEN 350

/* file related constants */
#define FILENAME "copy.txt"				/* extension appended to original file name for the copied file */
#define INIT_POS 0L

typedef struct fileType
{
	FILE * file;
	char fileName[MAX_FILENAME_LEN];
	unsigned blocksRead;
	unsigned nextBufBlock;				/* next line to be inserted/removed in the buffer */
	pthread_mutex_t mtxFile;			/* mutex controlling access to this file */
} file_t;

/* structure used to store the arguments to be passed to a thread routine upon creation */
typedef struct blockType
{
	char * mem;					/* location where thread stores its return code */
	int BLOCK_CAP;				/* the capacity of the block (also the amount of memory allocated to mem) */
	int BLOCK_SIZE;				/* the amount of data read from file into the block (the amount of data from mem to be written to file) */
} block_t;

typedef struct bufferType
{
	Semaphore semEmptyCount;
	Semaphore semFillCount;
	pthread_mutex_t mtxBuf;
	block_t ** circBuffer;
	int circStart;					/* Represents the front of the buffer(where data is read from) */
	int circEnd;					/* Represents the end of the buffer(where data is written to) */
	int circNextLineToBuffer;
	int circNextLineToFile;
	int count;
} buffer_t;

typedef struct timingType
{
	double totalWait;
	double run;
} timing_t;

typedef struct copierType
{
	/** Custom File structures **/
	file_t inFile;
	file_t outFile;
	buffer_t sharedBuf;
	timing_t* readerTimes;		/* array of pointers to thread timing structures (1 per reader) */
	timing_t* writerTimes;		/* array of pointers to thread timing structures (1 per writer) */
} copier_t;

/* structure used to store the arguments to be passed to a thread routine upon creation */
typedef struct threadArgsType
{
	copier_t * copy;			/* pointer to the structure of the program environment variables */
	int threadid;				/* Index that the thread will use to store its timing information in timing_t array */
	timing_t * timing;  		/* Timing struct */
	int DEBUG_ON; 				/* Passed as value to each thread to avoid the need to mutex the global debug flag */
	int t_rc;					/* location where thread stores its return code */
	int BLOCK_SIZE;
	int proc_ns;				/* The amount of time spent processing a block of data from file in nanoseconds (max 1s) */
} threadargs_t;


void* reader_thread1(void *arg);
void* reader_thread2(void *arg);
void* writer_thread(void *arg);

void initGlobals();
void initFile(char fileName[], file_t * file);
void initBuf(buffer_t * sharedBuf, int BUFFER_SIZE);
void addToBuffer(buffer_t * sharedBuf, block_t * line);
block_t * removeFromBuffer(buffer_t * sharedBuf);
int initTimings(timing_t ** readerTimes, timing_t ** writerTimes, int numReaders, int numWriters);
void freeBlock(block_t * block);

/* #endif */
