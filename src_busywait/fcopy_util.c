/******************************************************************************
*
*
*
* Full Name        : Christopher White
* Student Number   : s3233820
* Yallara Username : s3233820
* Course Code      : 
* Program Code     : BP097 - Bachelor of Computer Science
* 
******************************************************************************/

#include "fcopy.h"
#include "fcopy_util.h"

/******************************************************************************
* 							Semaphore initialisation routines
******************************************************************************/


/******************************************************************************
* 							File I/O functions
******************************************************************************/

/** Description: Reads a line from the specified file,
*		memory will be allocated from the line read and refernced
*		by lineFromBuffer
*
*	return value: char* pointing to the new string or NULL if the function
*		failed
*/
char * readLineFromFile(file_t * file){
	char tempBuffer[MAXBUF + 1];
	char * lineFromFile;
	
	/* get a line from file
	*/
	if ((fgets(tempBuffer, MAXBUF, (FILE *)file->file)) == NULL){
		
		if (feof(file->file) != NOERROR){
			/*printfLog("Failed to read new line, EOF reached\n");*/
			return NULL;
		}
		
		if (ferror(file->file) != NOERROR){
			printfLog("ERROR : function failed with errno = %i \n", errno);
			return NULL;
		}
	}
	
	/* allocate memory for string in buffer (MUST BE FREE'd) */
	if ((lineFromFile = (char * )mallocMem(strlen(tempBuffer) + 1)) == NULL){
		printfLog("Allocating Memory has failed\n");
		return NULL;
	}
	
	strcpy(lineFromFile, tempBuffer);
	
	return lineFromFile;
}

/* Write line to file */
int writeLineToFile(file_t * file, char * lineToFile){
	
	/* write line */
	if ((fputs(lineToFile, file->file)) == NULL){
		printfErr("Could not write line to file, reason: %s\n", strerror(errno));
		return FAILURE;
	}
	
	return SUCCESS;
}

/** Description:
*
*	return value: 
*/
block_t * readBlockFromFile(file_t * file, int BLOCK_SIZE){
	char * mem;
	block_t * block;
	
	/* allocate BLOCK_SIZE units of memory */
	if ((mem = mallocMem(BLOCK_SIZE)) == NULL){
		exit(EXIT_FAILURE);
	}
	
	/* allocate memory for the block struct */
	if ((block = mallocMem(sizeof(block_t))) == NULL){
		exit(EXIT_FAILURE);
	}
	
	/* initialise block_t structures memory */
	block->mem = mem;
	block->BLOCK_CAP = BLOCK_SIZE;
	block->BLOCK_SIZE = 0;
	
	/* read BLOCK_SIZE units from file */
	if ((block->BLOCK_SIZE = fread(block->mem, (size_t)sizeof(char), (size_t)block->BLOCK_CAP, file->file)) < block->BLOCK_CAP){
		
		/* check for error */
		if (ferror(file->file) != 0){
			printfLog("ERROR : an error occurred reading from the stream errno = %i \n", errno);
			exit(EXIT_FAILURE);
		}
	}
	
	/* return pointer to block */
	return block;
}

/* Write block to file */
int writeBlockToFile(file_t * file, block_t * block){
	
	if (fwrite(block->mem, (size_t)sizeof(char), (size_t)block->BLOCK_SIZE, file->file) != block->BLOCK_SIZE){
		printfErr("Could not write block to file, reason: %s\n", strerror(errno));
		return FAILURE;
	}
	
	return SUCCESS;
}

/****************************************************************************
* 						Memory handling functions
****************************************************************************/

/* other free()'ing and allocating funcs go here */

/* wrapper function: allocate's memory of the specified size,  */
void * mallocMem(int size){
	char * newMemory;
	
	/* allocate memory for this data */
	if ((newMemory = (char *)malloc(size)) == NULL){
		printfErr("Could not allocate sufficient memory, reason: %s\n", 
			strerror(errno));
		exit(EXIT_FAILURE);
	}

	return newMemory;
}

/****************************************************************************
* 					Text and display oriented functions
****************************************************************************/

/* Calls the print function value passed as first argument is true */
void printfDebug(int DEBUG, char * format, ...){
	va_list args;
	if (DEBUG){
		va_start (args, format);
		printIt(format, args);
	}
}

/* Called by printfDebug if DEBUG == TRUE */
void printIt(char * format, va_list args){
	char * stamp;
	
	stamp = timeStamp();
	printf("%s: ", stamp); 
	free(stamp);
	
	vprintf (format, args);
	va_end (args);
}


void printfErr (char * format, ...)
{
	va_list args;
	
	printf("\tERROR: ");
	va_start (args, format);
	vprintf (format, args);
	va_end (args);
	
	/* reset the error indicator */
	errno = RESET;
}

void printfLog (char * format, ...)
{
	va_list args;
	/*char * stamp;
	
	stamp = timeStamp();
	
	printf("%s: ", stamp);
	free(stamp); */
	va_start (args, format);
	vprintf (format, args);
	va_end (args);
}

/* Prints the time with format[hh:mm:ss], returns null on failure */
char * timeStamp()
{
	time_t calTime;
	struct tm *locTime;
	char * stamp;		/* change this to dynamic later */
	
	/* allocate memory for string */
	if ((stamp = malloc(MAXBUF)) == NULL){
		printfErr("Failed allocating memory in func() timeStamp()\n");
		return NULL;
	}
	
	calTime = time(NULL);
	locTime = localtime(&calTime);
	
	sprintf(stamp,"%d:%d:%d",
		locTime->tm_hour,
		locTime->tm_min,
		locTime->tm_sec);
	
	return stamp;
}

/* Prints a header-graphic (ASCII graphic) */
void headerGraphic()
{
	printf("\n");
	printf("        $$$$$$=     RRRRRRRRRRR    RRRRRR      RRRRRR  RRRRRRR RRRRRRRRRRRRR \n");
	printf("    $$$$$$$$$$$$$     MMM     MMM    MMMM     ,MMMM      MMM   M    MMM   MM \n");
	printf("    $$$$$$$$$$$$$$    III     III    I,III    I III      III   I    III    I \n");
	printf(" $$$$$$$$$$$$$$$$$$   TTT     TTT    T TTT    T TTT      TTT        TTT      \n");
	printf(" $$$$$$$$$$$$$$$$$$   RRR~~?RR~      R  RRR  R  RRR      RRR        RRR      \n");
	printf(" $$$$$$$$$$$$$$$$$$   MMM   MMM      M  MMM  M  MMM      MMM        MMM      \n");
	printf("  $$$$$$$$$$$$$$$$$   III    III     I   RMIT   III      III        III      \n");
	printf("    $$$$$$$$$$$$$7    TTT    TTTT    T   RMIT   TTT      TTT        TTT      \n");
	printf("    $$$$$$$$$$$$$     CCC    CCCC    C    CC    CCC      CCC        CCC      \n");
	printf("        $$$$$$=       SSS     SSS8   S    SS    SSS    SSSSSSS      SSS      \n");
	printf("                                                                             \n");
	printf(" Christopher White(s3233820), Nick Taylor(s3329578), Phillip (sX) - OSP Project \n\n");
}


